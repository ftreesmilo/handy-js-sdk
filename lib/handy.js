const CORE = require('./core/index.js');
const config = require('./config.js');
const state = require('./state.js');

class Handy {
  constructor(initialConfig = {}) {
    config.setInitialConfig(initialConfig);
  }

  /**
   * Start your Handy manually
   *
   * @param {Boolean} start True to start, false to stop.
   * @returns {Object} A promise with the result.
   */
  changeMode(start) {
    return new Promise((resolve, reject) => {
      const { api, defaultTimeout } = config.getConfig();
      const { connectionKey } = state.getState();
      CORE.setMode({ API: api, handyKey: connectionKey, mode: (start ? 1 : 0), timeout: defaultTimeout})
        .then(response => {
          if (response.success) {
            const uState = state.updateState({ connected: response.connected, mode: response.mode });
            resolve({ ...uState, message: `Handy is ${response.mode === 0 ? 'stopped' : 'moving'}` });
          } else {
            reject('Unexpected response');
          }
        })
        .catch(e => {
          reject(e);
        });
    });
  }

  /**
   * Check your Handy version.
   *
   * @returns {Object} A promise with the result.
   */
  checkVersion() {
    return new Promise((resolve, reject) => {
      const { api, defaultTimeout } = config.getConfig();
      const { connectionKey } = state.getState();
      CORE.getVersion({ API: api, endpoint: 'getVersion', handyKey: connectionKey, timeout: defaultTimeout })
        .then(deviceData => {
          state.updateState({ connected: !!deviceData.connected });

          if (!deviceData.success && deviceData.error) {
            reject(deviceData.error);
          } else if (deviceData.version && deviceData.version === deviceData.latest) {
            state.updateState({ deviceVersion: deviceData.version });
            resolve({ ...state.getState(), status: deviceData.status, message: 'Firmware is up to date' });
          } else {
            state.updateState({ deviceVersion: deviceData.version });
            const message = `Latest version: ${deviceData.latest}. Instaled: ${deviceData.version}`;
            resolve({ ...state.getState(), status: deviceData.status, message });
          }
        }).catch(e => reject(e));
    });
  }

  /**
   * Connect your Handy to the servers
   *
   * @param {String} key Handy key.
   * @returns {Object} A promise with the result.
   */
  connect(key, statusCallback) {
    return new Promise((resolve, reject) => {
      if (key?.length > 0) {
        this.setKey(key);
      } else if (CORE.isClientSide() && localStorage.connectionKey) {
        this.setKey(localStorage.connectionKey);
      }
      const { avgRtd } = state.getState();
      const { autoSync } = config.getConfig();
      if (autoSync && avgRtd < 0) {
        this.getServerLatency();
      }

      this.checkVersion()
        .then(response => {
          const { checkingStatus } = state.getState();
          if (!checkingStatus && statusCallback) {
            const intervalId = setInterval(() => {
              this.getStatus()
                .then(res => {
                  statusCallback(res);
                })
                .catch(e => {
                  statusCallback({...state.getState(), message: e});
                });
            }, 10000);
            state.updateState({ intervalId, checkingStatus: true });
          }
          resolve({ ...state.getState(), message: response.message });
        })
        .catch(e => {
          reject(e);
        });
    });
  }

  disconnect() {
    return new Promise((resolve, reject) => {
      const { intervalId } = state.getState();
      if (intervalId) {
        clearInterval(intervalId);
        state.updateState({ intervalId: null, checkingStatus: false });
      }

      if (CORE.isClientSide()) {
        localStorage.connectionKey = '';
      }

      this.stop()
        .then(() => {
          resolve({ ...state.updateState({ connectionKey: '', checkingStatus: false, connected: false }), message: 'Handy disconnected successfuly' });
        })
        .catch(e => {
          reject(e);
        });
    });
  }

  /**
   * Get the latency between your Handy and the server
   *
   * @returns {Object} Object containing the average offset and rtd.
   */
  async getServerLatency() {
    const { syncAttempts } = config.getConfig();
    const timeTable = [];
    while (timeTable.length < syncAttempts) {
      const times = await this.getServerRTDandOffset();
      if (times) {
        timeTable.push(times);
      }
    }

    const { aggOffset, aggRtd } = timeTable.reduce((prev, curr) => {
      if (curr.offset && curr.rtd) {
        prev.aggOffset += curr.offset;
        prev.aggRtd += curr.rtd;
      }
      return prev;
    }, { aggOffset: 0, aggRtd: 0 });

    const avgOffset = Math.round(aggOffset / timeTable.length);
    const avgRtd = Math.round(aggRtd / timeTable.length);
    state.updateState({ avgOffset, avgRtd });
    return ({ avgOffset, avgRtd });
  }

  /**
   * Get actual rtd and offset from server
   *
   * @returns {Object} A promise with the result containing the offset and rtd of the server.
   */
  getServerRTDandOffset() {
    return new Promise((resolve, reject) => {
      const { api } = config.getConfig();
      const sendTime = new Date();

      CORE.httpRequest({ API: api, endpoint: 'getServerTime', connectionKey: 'KEY' })
        .then(res => {
          const { serverTime } = res;

          if (serverTime) {
            const receiveTime = Date.now();
            const rtd = receiveTime - sendTime;
            const estimatedServerTimeNow = serverTime + rtd / 2;
            const offset = estimatedServerTimeNow - receiveTime;
            resolve({ rtd, offset });
          } else {
            resolve(null);
          }
        })
        .catch(e => {
          reject(e);
        });
    });
  }

  /**
   * Get status of your Handy
   *
   * @param {Number} timeout Desired timeout of the petition.
   * @returns {Object} Promise with the result.
   */
  getStatus(timeout) {
    return new Promise((resolve, reject) => {
      const { api, defaultTimeout } = config.getConfig();
      const { connectionKey } = state.getState();

      CORE.getStatus({ API: api, handyKey: connectionKey, timeout: (timeout || defaultTimeout) })
        .then(res => {
          const { success, serverTime, cmd, ...rest } = res;
          resolve({ ...state.updateState(rest), message: `Connected: ${rest.connected}` });
        })
        .catch(e => reject(e));
    });
  }

  /**
   * Get the connection key stored
   *
   * @returns {String} Connection key.
   */
  getStoredKey() {
    return state.getState().connectionKey || (CORE.isClientSide() && localStorage.connectionKey) || '';
  }

  /**
   * Get current time of the video
   *
   * @returns {Number} Current time.
   */
  getVideoTime() {
    const { videoId } = state.getState();
    const videoElement = CORE.isClientSide() ? document.getElementById(videoId) : null;
    if (videoElement && videoElement.tagName === 'VIDEO') {
      return Math.round(videoElement.currentTime * 1000);
    }
    return 0;
  }

  /**
   * Play action of the video
   *
   * @returns {Object} Promise with the result.
   */
  playVideo() {
    return new Promise((resolve, reject) => {
      const { api, defaultTimeout } = config.getConfig();
      const { connectionKey, avgOffset } = state.getState();
      const serverTime = Date.now() + avgOffset;
      const time = this.getVideoTime();
      CORE.syncPlay({ API: api, handyKey: connectionKey, serverTime, time, play: true, timeout: defaultTimeout })
        .then(response => {
          if (response.success) {
            const { cmd, success, ...rest} = response;
            resolve({ ...state.updateState(rest), message: `Playing: ${rest.playing}` });
          } else {
            reject('Request failed');
          }
        })
        .catch(e => {
          reject(`Error playing the content: ${e}`);
        })
    })
  }

  /**
   * Pause action of the video
   *
   * @returns {Object} Promise with the result.
   */
  pauseVideo() {
    return new Promise((resolve, reject) => {
      const { api, defaultTimeout } = config.getConfig();
      const { connectionKey } = state.getState();
      CORE.syncPlay({ API: api, handyKey: connectionKey, play: false, timeout: defaultTimeout })
        .then(response => {
          if (response.success) {
            const { cmd, success, ...rest} = response;
            resolve({ ...state.updateState(rest), message: `Playing: ${rest.playing}` });
          } else {
            reject('Request failed');
          }
        })
        .catch(e => {
          reject(`Error pausing the content: ${e}`);
        })
    });
  }

  /**
   * Set your Handy key on the state and, if is available, localstorage
   *
   * @param {String} key Handy key
   * @returns {void}
   */
  setKey(key) {
    state.updateState({ connectionKey: key });
    const { storeKey } = config.getConfig();
    if (storeKey && CORE.isClientSide()) localStorage.connectionKey = key;
  }

  /**
   * Sets the machine up for video sync
   *
   * @param {String} url URL for the machine to download the CSV file.
   * @param {String} name A name of the file.
   * @param {Number} size The size of the file in bytes.
   * @returns {Object} Promise with the result.
   */
  setScript(url, name, size) {
    return new Promise((resolve, reject) => {
      const { api } = config.getConfig();
      const { connectionKey } = state.getState();
      const params = { url };
      if (name) params.name = name;
      if (size) params.size = size;

      CORE.syncPrepare({ API: api, handyKey: connectionKey, ...params, timeout: 45000 })
        .then(response => {
          if (response.success && response.downloaded) {
            const { isVideoPlaying } = state.updateState({ scriptIsSet: true });
            if (isVideoPlaying) {
              this.playVideo()
                .then(() => resolve({ ...state.getState(), message: 'Script setted correctly' }))
                .catch(e => reject(e));
            } else {
              resolve({ ...state.getState(), message: 'Script setted correctly' });
            }
          } else {
            reject(response);
          }
        })
        .catch(e => {
          reject(`Error setting the script: ${e}`);
        })
    });
  }

   /**
   * Set the speed of the machine in %.
   *
   * @param {Number} speed 0-100.
   * @returns {Object} Promise with the result.
   */
  setSpeed(speed) {
    if (state.getState().actualSpeed === speed) {
      return new Promise((res, reject) => reject('SAME SPEED'));
    } else {
      state.updateState({ lastSpeedReq: speed });
    }

    if (state.getState().limitSpeedReq) return new Promise((res, reject) => reject('REQUESTS LIMITED'));

    return new Promise((resolve, reject) => {
      const { api, defaultTimeout, timeLimitation } = config.getConfig();
      const { connectionKey } = state.getState();

      setTimeout(() => {
        const { lastSpeedReq, actualSpeed } = state.getState();
        state.updateState({ limitSpeedReq: false });
        if (Number.parseFloat(actualSpeed) !== Number.parseFloat(lastSpeedReq)) this.setSpeed(lastSpeedReq);
      }, timeLimitation);

      const limitedSpeed = CORE.limit(speed);
      state.updateState({ actualSpeed: `${limitedSpeed}`, limitSpeedReq: true });

      CORE.setSpeed({ API: api, handyKey: connectionKey, speed: limitedSpeed, type: '%', timeout: defaultTimeout })
        .then(response => {
          if (response.success) {
            const { cmd, success, ...rest } = response;
            resolve({ ...state.updateState(rest), message: 'Speed changed successfully' });
          } else {
            reject(response);
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
   * Sets the machines stroke length in percent
   *
   * @param {Number} stroke 0-100.
   * @returns {Object} Promise with the result.
   */
  setStrokeLength(stroke) {
    if (state.getState().actualStroke === stroke) {
      return new Promise((res, rej) => rej('SAME STROKE'));
    } else {
      state.updateState({ lastStrokeReq: stroke });
    }

    if (state.getState().limitStrokeReq) return new Promise((res, rej) => rej('REQUESTS LIMITED'));

    return new Promise((resolve, reject) => {
      const { api, defaultTimeout, timeLimitation } = config.getConfig();
      const { connectionKey } = state.getState();

      setTimeout(() => {
        const { lastStrokeReq, actualStroke } = state.getState();
        state.updateState({ limitStrokeReq: false });
        if (Number.parseFloat(actualStroke) !== Number.parseFloat(lastStrokeReq)) this.setStrokeLength(lastStrokeReq);
      }, timeLimitation);

      const limitedStroke = CORE.limit(stroke);
      state.updateState({ actualStroke: `${limitedStroke}`, limitStrokeReq: true });

      CORE.setStroke({ API: api, handyKey: connectionKey, stroke: limitedStroke, type: '%', timeout: defaultTimeout})
        .then(response => {
          if (response.success) {
            const { cmd, success, ...rest } = response;
            resolve({ ...state.updateState(rest), message: 'Stroke changed successfully' });
          } else {
            reject(response);
          }
        })
        .catch(e => {
          reject(e);
        })
    });
  }

  /**
   * Sets the video element
   *
   * @param {Element} videoElement Element <video>.
   * @returns {void}
   */
  setVideoElement(videoElement) {
    if (videoElement && videoElement.tagName === 'VIDEO') {
      state.updateState({ videoId: videoElement.id, isVideoPlaying: videoElement.paused === false });
      videoElement.onplaying = () => {
        state.updateState({ isVideoPlaying: true });
        const { scriptIsSet } = state.getState();
        if (scriptIsSet) {
          this.playVideo();
        }
      }

      videoElement.onpause = () => {
        state.updateState({ isVideoPlaying: false });
        this.pauseVideo();
      }
    } else {
      console.log('setVideoElement: The element is not a video')
    }
  }

  /**
   * Sets the machine mode to stop
   *
   * @returns {Object} Promise with the result.
   */
  stop() {
    return new Promise((resolve, reject) => {
      const { api } = config.getConfig();
      const { connectionKey } = state.getState();
      CORE.setMode({ API: api, handyKey: connectionKey, mode: 0 })
        .then(response => {
          if (response.success && response.mode === 0) {
            resolve({ ...state.updateState({ mode: response.mode }), message: 'Handy stopped successfuly' });
          } else {
            reject('The Handy was not stopped');
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * The machine will start reading the CSV file and do the commands in the file at the time specified.
   *
   * @param {Number} time Where should the sync start at [ms].
   * @param {Number} serverTime The current estimated server time when sending the message.
   * @returns {Object} Promise with the result.
   */
  syncPlay(time, serverTime) {
    return new Promise((resolve, reject) => {
      const { api, defaultTimeout } = config.getConfig();
      const { connectionKey } = state.getState();
      const params = {};
      if (serverTime !== 0) {
        params.serverTime = serverTime;
      }

      CORE.syncPlay({ API: api, handyKey: connectionKey, play: true, time, timeout: defaultTimeout, ...params })
        .then(response => {
          if (response.success) {
            const { cmd, success, ...rest} = response;
            resolve({ ...state.updateState(rest), message: `Playing: ${rest.playing}` });
          } else {
            reject('Request failed');
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * The machine will adjust on the fly the missmatch between client videoplayer current time and Handys current video play time
   *
   * @param {Number} currentTime Where should the sync start at [ms].
   * @param {Number} serverTime The current estimated server time when sending the message.
   * @param {Number} filter [optional] How hard the sync adjust should be. (default = 0.5)
   * @returns {Object} Promise with the result.
   */
  syncAdjustTimestamp(currentTime, serverTime, filter = 0.5) {
    return new Promise((resolve, reject) => {
      const { api, defaultTimeout } = config.getConfig();
      const { connectionKey } = state.getState();
      const params = {};

      CORE.syncAdjustTimestamp({ API: api, handyKey: connectionKey, serverTime, currentTime, filter, timeout: defaultTimeout, ...params })
        .then(response => {
          if (response.success) {
            const { cmd, success, ...rest} = response;
            resolve({ ...state.updateState(rest), message: `Playing: ${rest.playing}` });
          } else {
            reject('Request failed');
          }
        })
        .catch(e => {
          reject(e);
        })
    })
  }

  /**
   * Upload funscript or csv to Handy servers
   *
   * @param {Element} data File element.
   * @returns {Object} Promise with the result.
   */
  uploadData(file) {
    return new Promise((resolve, reject) => {
      const { uploadAPI } = config.getConfig();

      CORE.readFile(file)
        .then((fd) => {
          CORE.uploadFile({ API: uploadAPI, data: fd })
            .then(response => {
              const { connected } = state.getState();
              if (response.success && connected) {
                this.setScript(response.url, response.filename, response.size)
                  .then(() => {
                    resolve({...state.updateState({ url: response.url }), message: `${response.orginalfile} - ${response.info}`});
                  })
                  .catch(err => {
                    reject(err);
                  });
              } else if (response.success) {
                resolve({...state.updateState({ url: response.url }), message: `${response.orginalfile} - ${response.info}`});
              } else {
                reject(response);
              }
            })
            .catch(e => {
              reject(e);
            });
        })
        .catch(err => {
          reject(err);
        })
    })
  }
}

exports.init = configuration => new Handy(configuration);
