const API = {
  PRODUCTION: "https://www.handyfeeling.com/api/v1",
  STAGING: "https://staging.handyfeeling.com/api/v1",
  LOCAL: "http://localhost:3000/api/v1"
}

const config = {
  api: API.PRODUCTION,
  autoSync: true,
  uploadAPI: 'https://handyfeeling.com/api/sync/upload',
  defaultTimeout: 5000,
  storeKey: true,
  syncAttempts: 30,
  timeLimitation: 500
};

const setHandyKey = (value) => {
  config.connectionKey = value;
}

const setInitialConfig = ({ ENV, autoSync = true, defaultTimeout = 5000, storeKey = true, syncAttempts = 30 }) => {
  config.api = API[ENV] || API.PRODUCTION;
  config.autoSync = autoSync;
  config.defaultTimeout = defaultTimeout;
  config.storeKey = storeKey;
  config.syncAttempts = syncAttempts;
}

const getConfig = () => {
  return config;
}

module.exports = {
  setHandyKey,
  setInitialConfig,
  getConfig
}