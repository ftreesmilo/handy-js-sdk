const deviceInfo = require('./deviceInfo.js');
const manipulation = require('./manipulation.js');
const utils = require('./utils.js');
const videoSync = require('./videoSync.js');

module.exports = { ...deviceInfo, ...manipulation, ...utils, ...videoSync };