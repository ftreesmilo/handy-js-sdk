const utils = require('./utils.js');

const setMode = ({API, handyKey, mode = 0, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'setMode', connectionKey: handyKey, mode, timeout });
}

const toggleMode = ({ API, handyKey, mode = 0, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'toogleMode', connectionKey: handyKey, mode, timeout });
}

const setSpeed = ({ API, handyKey, speed, type, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'setSpeed', connectionKey: handyKey, speed, type, timeout });
}

const setStroke = ({ API, handyKey, stroke, type, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'setStroke', connectionKey: handyKey, stroke, type, timeout });
}

const stepSpeed = ({ API, handyKey, step, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'stepSpeed', connectionKey: handyKey, step, timeout });
}

const stepStroke = ({ API, handyKey, step, timeout }) => {
  return utils.httpRequest({ API, endpoint: 'stepStroke', connectionKey: handyKey, step, timeout });
}

module.exports = {
  setMode,
  toggleMode,
  setSpeed,
  setStroke,
  stepSpeed,
  stepStroke
}