const state = {
  actualSpeed: "-1",
  actualStroke: "-1",
  avgOffset: -1,
  avgRtd: -1,
  checkingStatus: false,
  connected: false,
  connectionKey: "",
  deviceVersion: "",
  intervalId: null,
  isVideoPlaying: false,
  lastSpeedReq: "-1",
  lastStrokeReq: "-1",
  limitSpeedReq: false,
  limitStrokeReq: false,
  mode: 0,
  playing: false,
  position: -1,
  scriptIsSet: false,
  setOffset: -1,
  setSpeedPercent: -1,
  setStrokePercent: -1,
  speed: -1,
  url: "",
  videoId: "",
  adjustment: -1,
  adjustmentTotalt: -1,
  serverDelta: -1,
}

const getState = () => { return state };

const updateState = (value) => {
  Object.keys(value)
    .forEach(key => {
      state[key] = value[key];
    });
  return state;
}

module.exports = {
  getState,
  updateState
}