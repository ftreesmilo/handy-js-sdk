# The Handy JS SDK

### Config variables
- ENV: string. This value sets which API backend environment is used for API requests. Accepted values are: PRODUCTION, STAGING and LOCAL. The default value is PRODUCTION.
- autoSync: boolean. If this value is true, the latency between the Handy and the Handy server will be calculated on calling of the `connect` method. The default value is true.
- defaultTimeout: number. This value sets the timeout (in milliseconds) for API requests. The default value is 5000 ms.
- storeKey: boolean. If this value is true, we try to save your connection key in your browser's localStorage. The default value is true.
- syncAttempts: number. This value sets the number of request to be performed in order to calculate the average latency between your computer and your Handy. The default value is 30.

### Usage on web browsers
- Include the `handy-1.0.0.js` in your document's `<head>` tag
- After that, initialize the library inside a script tag `const HANDY = Handy();`. You can also set custom config (see above)
- Call the methods available

### Usage on Node apps
- Import the library `const lib = require('./lib/handy.js');`
- After that, initialize the library `const HANDY = lib.init();` you can set a custom config (see above)
- Call the available methods

### Available methods
Ordered alphabetically.

##### changeMode
Sets your Handy mode. Handy has two modes: running and stopped. Running is active mode where the Handy moves and responds to commands. Stopped is where the Handy does not move.

Params:
- start: boolean. Set to true to put Handy into running mode. Set to false to put Handy into stopped mode.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### checkVersion
Check your Handy firmware version.

Params:
- None

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### connect
Connect your Handy to the Handy servers.

Params:
- key (optional): string. A Handy key. If you don't send the key, the library will try to get a key from the localStorage.
- statusCallback (optional): Function. If you call `connect` with the second argument you will get an object with the state of the Handy every 10 seconds.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### disconnect
Disconnect your Handy from the Handy servers, and remove the connection key from the state and localStorage.

Params:
- None

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### getServerLatency
Get the average latency between your Handy and the Handy server.

Params:
- checkNmb (optional): number. Number of latency requests to make to the server. Default is 30.

Return:
- Promise. The request will return the following object { avgOffset, avgRtd }.

##### getServerRTDandOffset
Get actual round-trip-delay (rtd) and offset from the Handy server.

Params:
- None

Return:
- Promise. The request will return the following object { rtd, offset }. On failure, it will return a string with the error.

##### getStatus
Get the status of your Handy.

Params:
- timeout (optional): number. Desired timeout for the request in milliseconds.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### getStoredKey
Get the connection key stored in the state or localStorage.

Params:
- None

Return:
- String. Your stored Handy key. Returns an empty string when there is no stored key.

##### getVideoTime
Get current time of the video.

Params:
- None

Return:
- Number. Returns the current time of the video. 0 by default.

##### pauseVideo
Pause the video.

Params:
- None

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### playVideo
Play action of the video

Params:
- None

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### setKey
Set your Handy key in the state and, if available, in localStorage.

Params:
- key: string. Your Handy key.

Return:
- None

##### setScript
Prepares the Handy for video sync.

Params:
- url: string. URL for the Handy to download the CSV file.
- name (optional): string. The name of the file.
- size (optional): string. The size of the file in bytes.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### setSpeed
Set the speed of the Handy in %. Valid speeds are between 0 and 100. Note: 0% speed does not stop the Handy; rather, it makes it move at the slowest possible speed.

Params:
- speed: number. New speed of the Handy from 0 to 100.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### setStrokeLength
Sets the Handy's stroke length in percent. Valid stroke lengths are between 0 and 100. Note: 0% stroke does not stop the Handy; rather, it makes it move in the smallest possible stroke length.

Params:
- stroke: number. New stroke of the Handy from 0 to 100.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### setVideoElement
Sets the video element and its onplaying and onpause events.

Params:
- videoElement: Element<video>. DOM element of the video.

Return:
- None

##### stop
Sets the Handy mode to stop.

Params:
- None

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### syncAdjustTimestamp
The Handy will adjust on the fly the mismatch between the client video player's current time and the Handy's current video play time.

Params:
- time: number. Where should the sync start at in milliseconds.
- serverTime: number. The current estimated server time when sending the message.
- filter (optional): number. How hard the sync adjust should be. (default = 0.5)

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### syncPlay
The Handy will start reading the CSV file and execute the commands within the file at the time specified.

Params:
- time: number. The time in milliseconds where the sync should start.
- serverTime: number. The current estimated server time when sending the message.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

##### uploadData
Upload a FunScript, CSV file or a FunScript JSON to Handy servers. If the Handy is connected to the server, it also sets the script.

Params:
- { file, json }. One of the two keys are required. The property file needs to be a DOM file for the client side and a path to the file for Node.js apps. The JSON property needs to be a JSON with FunScript format.

Return:
- Promise. The request will return an object containing the status and a detailed message of the response on the key 'message'. On failure, it will return a string with the error.

### Response Example

|key|value|
|-|-|
|actualSpeed|string. Handy speed in %.|
|actualStroke|string. Handy stroke in %.|
|avgOffset|number. Average offset.|
|avgRtd|number. Average round-trip delay.|
|checkingStatus|boolean. Shows if you enabled the periodic 10-second callback in the connect method.|
|connected|boolean. True if your Handy is connected.|
|connectionKey|string. Your connection key.|
|deviceVersion|string. Your device firmware version.|
|isVideoPlaying|boolean. True if the video is playing.|
|lastSpeedReq|string. Shows the value of the last set speed request in %.|
|lastStrokeReq|string. Shows the value of the last set stroke request in %.|
|limitSpeedReq|boolean. Shows if the requests are limited due to a continuous changes in a short period of time.|
|limitStrokeReq|boolean. Shows if the requests are limited due to a continuous changes in a short period of time.|
|mode|number. Shows the mode of your Handy.|
|playing|boolean. True if the video is playing.|
|position|number. Current position of the slider in mm.|
|scriptIsSet|boolean. True if the script has been set.|
|setOffset|number. The current offset of the playTime on the machine versus the video player.|
|setSpeedPercent|number. Current relative machine speed (% of max).|
|setStrokePercent|number. Current machine stroke setting in mm.|
|speed|number. Current absolute speed (average over 5000 ms) in mm/s.|
|url|string. URL of the loaded script file.|
|videoId|string. Id of the video element.|
|message|string. Last message for the user.|

### Dev
- `npm i` to install dependencies.
- `npm run build` to build the bundle for web browsers.
